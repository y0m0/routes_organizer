import React, { Component } from 'react';
import Header from './components/Header';
import Board from './components/Board';

import './custom-style.css';

class App extends Component {
  render() {
    return (
      <div>
        <Header />
        <Board />
      </div>
    );
  }
}

export default App;
