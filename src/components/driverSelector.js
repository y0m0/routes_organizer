import React, { Component } from 'react';
import { connect } from 'react-redux';
import Dropdown from 'react-dropdown';
import 'react-dropdown/style.css';

import { assignDriver } from '../actions';

const drivers = [
  { value: "Driver", label: "Driver" },
  { value: "Edvinas", label: "Edvinas" },
  { value: "Glenn", label: "Glenn" },
  { value: "Kassem", label: "Kassem" },
  { value: "Thodoris", label: "Thodoris" },
  { value: "Andrea", label: "Andrea" },
  { value: "Roberto", label: "Roberto" },
  { value: "Maurizio", label: "Maurizio" },
];


class DriverSelector extends Component {
  render() {
    const { routes, routeName } = this.props;
    const route = routes[routes.findIndex(x => x.routeName === routeName)];

    return (
      <Dropdown
        options={drivers}
        placeholder="Driver"
        value={route.driver}
        onChange={selection => {this.props.dispatch(assignDriver(routeName, selection.value))}}
      />
    );
  }
}

const mapStateToProps = (state) => {
  return {
    routes: state.routes
  }
}

export default connect(mapStateToProps)(DriverSelector);
