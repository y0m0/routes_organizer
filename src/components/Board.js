import React, { Component } from 'react';

import SearchBox from './SearchBox';
import RouteList from './RouteList';


class Board extends Component {
  render() {
    return (
      <div className="container">
        <SearchBox />
        <RouteList />
      </div>
    );
  }
}

export default Board;
