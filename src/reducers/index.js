import {
  ASSIGN_DRIVER,
  ADD_CUSTOMER,
  REMOVE_CUSTOMER,
  MOVE_CUSTOMER,
  CLEAN_ALL
} from '../constants/actionTypes.js';

const initialState = {
  routes: [
    {
      routeName: "unassigned",
      customers: []
    },
    {
      routeName: "collect",
      driver: "",
      customers: []
    },
    {
      routeName: "west morning",
      time: "morning",
      driver: "",
      customers: []
    },
    {
      routeName: "center morning",
      time: "morning",
      driver: "",
      customers: []
    },
    {
      routeName: "east morning",
      time: "morning",
      driver: "",
      customers: []
    },
    {
      routeName: "long trip morning",
      time: "morning",
      driver: "",
      customers: []
    },
    {
      routeName: "west afternoon",
      time: "afternoon",
      driver: "",
      customers: []
    },
    {
      routeName: "center afternoon",
      time: "afternoon",
      driver: "",
      customers: []
    },
    {
      routeName: "east afternoon",
      time: "afternoon",
      driver: "",
      customers: []
    },
    {
      routeName: "long trip afternoon",
      time: "afternoon",
      driver: "",
      customers: []
    }
  ]
};

const rootReducer = (state = initialState, action) => {
  switch(action.type) {
    case ASSIGN_DRIVER:
      const index = state.routes.findIndex(x => x.routeName === action.route);

      return { ...state,
        routes: [
          ...state.routes.slice(0, index),
          Object.assign({}, state.routes[index], { driver: action.driver }),
          ...state.routes.slice(index + 1)
        ]
      };

    case ADD_CUSTOMER:
      const addIndex = state.routes.findIndex(x => x.routeName === action.route);

      return { ...state,
        routes: [
          ...state.routes.slice(0, addIndex),
          Object.assign({}, state.routes[addIndex], { customers: [ ...state.routes[addIndex].customers, action.customer ] }),
          ...state.routes.slice(addIndex + 1)
        ]
      };

    case REMOVE_CUSTOMER:
      const removeIndex = state.routes.findIndex(x => x.routeName === action.route);
      const updatedCustomers = state.routes[removeIndex].customers.filter(c => c.id !== action.customerId)

      return { ...state,
        routes: [
          ...state.routes.slice(0, removeIndex),
          Object.assign({}, state.routes[removeIndex], { customers: updatedCustomers }),
          ...state.routes.slice(removeIndex + 1)
        ]
      };

   case MOVE_CUSTOMER:
      const { route, fromIndex, toIndex, payload } = action;
      const routeIndex = state.routes.findIndex(x => x.routeName === route);

      // move within the same list
      if (fromIndex !== null && toIndex !== null) {
        const newCustomers = Array.from(state.routes[routeIndex].customers);
        const [removedCustomer] = newCustomers.splice(fromIndex, 1);
        newCustomers.splice(toIndex, 0, removedCustomer);
        return {
          ...state,
          routes: [
            ...state.routes.slice(0, routeIndex),
            Object.assign({}, state.routes[routeIndex], { customers: newCustomers }),
            ...state.routes.slice(routeIndex + 1)
          ]
        }
      }

      // Move card from one list to another
      // Add to target list
      if (fromIndex == null && toIndex !== null) {
        const targetCustomers = [ ...state.routes[routeIndex].customers ]
        targetCustomers.splice(toIndex, 0, payload);
        return {
          ...state,
          routes: [
            ...state.routes.slice(0, routeIndex),
            Object.assign({}, state.routes[routeIndex], { customers: targetCustomers }),
            ...state.routes.slice(routeIndex + 1)
          ]
        }
      }

      // Remove from starting list
      if (fromIndex !== null && toIndex == null) {
        const sourceCustomers = [ ...state.routes[routeIndex].customers ]
        sourceCustomers.splice(fromIndex, 1);
        return {
          ...state,
          routes: [
            ...state.routes.slice(0, routeIndex),
            Object.assign({}, state.routes[routeIndex], { customers: sourceCustomers }),
            ...state.routes.slice(routeIndex + 1)
          ]
        }
      }

      break;
    case CLEAN_ALL:
      return initialState

    default:
      return state;
  }
};

export default rootReducer;
