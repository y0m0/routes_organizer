import React, { Component } from 'react';
import { connect } from 'react-redux';
import SearchBox from './SearchBox';

import { cleanAll } from '../actions';

class Header extends Component {
  render() {
    return (
      <nav>
        <div className="nav-wrapper">
          <a className="brand-logo" style={{paddingLeft: "15px"}}>Routes Organizer</a>
          <ul id="nav-mobile" className="right hide-on-med-and-down">
            <li><a>Print</a></li>
            <li><a onClick={() => this.props.dispatch(cleanAll())}>Clean</a></li>
          </ul>
        </div>
      </nav>
    );
  }
}


export default connect()(Header);
