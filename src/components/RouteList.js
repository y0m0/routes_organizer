import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Container, Draggable } from 'react-smooth-dnd';
import DriverSelector from './driverSelector';

import { removeCustomer, moveCustomer } from '../actions';

class RouteList extends Component {
  renderRoute(time) {
    return this.props.routes.filter(x => x.time === time).map(route => {
      return (
        <div className="col s3" key={route.routeName}>
          <div className="list">
          <div style={{ display: "flex", alignItems: "center", justifyContent: "space-between" }}>
            <div style={{flex: 4, fontSize: "1.2rem", textTransform: "capitalize"}}>
              {route.routeName.split(" ")[0]}
            </div>
            <div style={{flex: 1, fontSize: "0.8rem"}}>
              <DriverSelector routeName={route.routeName} />
            </div>
          </div>
          <Container
            groupName="routes"
            getChildPayload={i => this.getCardPayload(route.routeName, i)}
            onDrop={e => this.onCardDrop(route.routeName, e)}
            dragClass="opacity-ghost"
            dropClass="opacity-ghost-drop"
          >
            {
              route.customers.map(customer => {
                return (
                  <Draggable className="card hoverable"
                    key={customer.id}
                    style={{cursor: "pointer"}}
                  >
                    <div className="card-content" style={{ display: "flex", justifyContent: "space-between", alignItems: "center" }}>
                      <p>{customer.name}</p>
                      <a className="waves-effect waves-light"
                        onClick={() => this.props.dispatch(removeCustomer(route.routeName, customer.id))}
                      >
                        <i className="material-icons" style={{color: "red"}}>delete</i>
                      </a>
                    </div>
                  </Draggable>
                );
              })
            }
          </Container>
        </div>
      </div>
      );
    })}

  renderUnassigned() {
    const unassigned = this.props.routes[0];

      return (
          <div className="list unassigned red lighten-2">
          <Container
            groupName="routes"
            getChildPayload={i => this.getCardPayload(unassigned.routeName, i)}
            onDrop={e => this.onCardDrop(unassigned.routeName, e)}
            dragClass="opacity-ghost"
            dropClass="opacity-ghost-drop"
          >
            {
              unassigned.customers.map(customer => {
                return (
                  <Draggable className="card hoverable"
                    key={customer.id}
                    style={{cursor: "pointer"}}
                  >
                    <div className="card-content" style={{ display: "flex", justifyContent: "space-between", alignItems: "center" }}>
                      <p>{customer.name}</p>
                      <a className="waves-effect waves-light"
                        onClick={() => this.props.dispatch(removeCustomer(unassigned.routeName, customer.id))}
                      >
                        <i className="material-icons" style={{color: "red"}}>delete</i>
                      </a>
                    </div>
                  </Draggable>
                );
              })
            }
          </Container>
        </div>
      );
  }

  renderCollect() {
    const collect = this.props.routes[1];

      return (
          <div className="list collect">
          <Container
            groupName="routes"
            getChildPayload={i => this.getCardPayload(collect.routeName, i)}
            onDrop={e => this.onCardDrop(collect.routeName, e)}
            dragClass="opacity-ghost"
            dropClass="opacity-ghost-drop"
          >
            {
              collect.customers.map(customer => {
                return (
                  <Draggable className="card hoverable"
                    key={customer.id}
                    style={{cursor: "pointer"}}
                  >
                    <div className="card-content" style={{ display: "flex", justifyContent: "space-between", alignItems: "center" }}>
                      <p>{customer.name}</p>
                      <a className="waves-effect waves-light"
                        onClick={() => this.props.dispatch(removeCustomer(collect.routeName, customer.id))}
                      >
                        <i className="material-icons" style={{color: "red"}}>delete</i>
                      </a>
                    </div>
                  </Draggable>
                );
              })
            }
          </Container>
        </div>
      );
  }
  getCardPayload = (routeName, index) => {
    return this.props.routes.filter(r => r.routeName === routeName)[0].customers[
      index
    ];
  }

  onCardDrop = (routeName, dropResult) => {
    if (dropResult.removedIndex !== null || dropResult.addedIndex !== null) {
      this.props.dispatch(moveCustomer(routeName, dropResult));
    }
  }

  render() {
    return (
      <div className="row">
        <div className="col s2" style={{ display: "flex", flexDirection: "column" }}>
          <div className="section">
          <h5>Unassigned</h5>
          {this.renderUnassigned()}
        </div>
        </div>
        <div className="col s8">
          <div className="section">
            <h5>Morning</h5>
            <div className="row">
              {this.renderRoute("morning")}
            </div>
          </div>

          <div className="divider"></div>
          <div className="section">
            <h5>Afternoon</h5>
            <div className="row">
              {this.renderRoute("afternoon")}
            </div>
          </div>
        </div>

        <div className="col s2">
          <div className="section">
            <h5>Collect</h5>
            <div className="row">
              {this.renderCollect()}
            </div>
          </div>
        </div>

      </div>
    );
  }
}


const mapStateToProps = (state) => {
  return {
    routes: state.routes
  }
}

export default connect(mapStateToProps)(RouteList);
