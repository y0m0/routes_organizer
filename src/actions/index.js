import {
  ASSIGN_DRIVER,
  ADD_CUSTOMER,
  REMOVE_CUSTOMER,
  MOVE_CUSTOMER,
  CLEAN_ALL
} from '../constants/actionTypes';

export const assignDriver = (route, driver) => {
  return {
    type: ASSIGN_DRIVER,
    route,
    driver
  }
}

export const addCustomer = (route, customer) => {
  return {
    type: ADD_CUSTOMER,
    route: route,
    customer: customer
  }
};

export const removeCustomer = (route, customerId) => {
  return {
    type: REMOVE_CUSTOMER,
    route: route,
    customerId: customerId
  }
};

export const moveCustomer = (route, moveResult) => {
  return {
    type: MOVE_CUSTOMER,
    route,
    fromIndex: moveResult.removedIndex,
    toIndex: moveResult.addedIndex,
    payload: moveResult.payload
  }
}

export const cleanAll = () => {
  return {
    type: CLEAN_ALL
  }
}
